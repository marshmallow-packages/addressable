<?php

return [
	'wysiwyg' => env('NOVA_WYSIWYG', \Laravel\Nova\Fields\Trix::class),
];
